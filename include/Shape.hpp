#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <string>

class Shape
{
   public:
    virtual double      perimeter() = 0;
    virtual double      area()      = 0;
    virtual std::string show()      = 0;
};

#endif  // SHAPE_PP
