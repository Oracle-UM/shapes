#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "Shape.hpp"

#include <string>

class Rectangle : public Shape
{
   public:
    Rectangle(double width, double height);
    double      perimeter() override;
    double      area()      override;
    std::string show()      override;

   protected:
    bool isSquare();
    double _width;
    double _height;

   private:
    double __perimeter;     // Cached by perimeter()
    double __area;          // Cached by area()
    int    __is_square;     // Cached be isSquare()
};

#endif  // RECTANGLE_HPP
