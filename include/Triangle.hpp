#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "Shape.hpp"

#include <string>
#include <tuple>

class Triangle : public Shape
{
   public:
    enum class SideType
    {
        Scalene,
        Isosceles,
        Equilateral
    };
    enum class AngleType
    {
        Acute,
        Right,
        Obtuse
    };
    Triangle(double side_a, double side_b, double side_c);
    double                             perimeter() override;
    double                             area()      override;
    std::string                        show()      override;
    std::tuple<double, double, double> sides() const;
    std::tuple<double, double, double> angles();
    SideType                           sideType();
    AngleType                          angleType();

   protected:
    std::string showSideType();
    std::string showAngleType();
    double _side_a;
    double _side_b;
    double _side_c;

   private:
    double    __perimeter;    // Cached by perimeter()
    double    __area;         // Cached by area()
    double    __angle_ab;     // Cached by angles()
    double    __angle_ac;     // Cached by angles()
    double    __angle_bc;     // Cached by angles()
    SideType  __side_type;    // Cached by sideType()
    AngleType __angle_type;   // Cached by angleType()
};

#endif  // TRIANGLE_HPP
