#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "Shape.hpp"

#include <string>

class Circle : public Shape
{
   public:
    Circle(double radius);
    double      perimeter() override;
    double      area()      override;
    std::string show()      override;

   protected:
    double _radius;

   private:
    double __perimeter;  // Cached by perimeter()
    double __area;       // Cached by area()
};

#endif  // CIRCLE_HPP
