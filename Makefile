CXX            = clang++
SRC_EXT        = .cpp
HEAD_EXT       = .hpp
WARN_FLAGS     = --std=c++17 -pedantic -W -Wall
OPTIMIZE_FLAGS = -O2 -march=native
DEBUG_FLAGS    = -g

SOURCE_DIR = src
HEADER_DIR = include
BUILD_DIR  = build
DEBUG_DIR  = debug
DOC_DIR    = docs
EXECUTABLE = Shapes.elf

SOURCES = $(shell find $(SOURCE_DIR) -name '*$(SRC_EXT)')
HEADERS = $(shell find $(HEADER_DIR) -name '*$(HEAD_EXT)')

BUILD_OBJECTS = $(patsubst $(SOURCE_DIR)/%$(SRC_EXT), $(BUILD_DIR)/%.o, $(SOURCES))
DEBUG_OBJECTS = $(patsubst $(SOURCE_DIR)/%$(SRC_EXT), $(DEBUG_DIR)/%.o, $(SOURCES))

all: build_dir $(BUILD_DIR)/$(EXECUTABLE)

debug: debug_dir $(DEBUG_DIR)/$(EXECUTABLE)

memcheck:
	valgrind --tool=memcheck --leak-check=yes ./$(BUILD_DIR)/$(EXECUTABLE)

docs: $(HEADERS)
	doxygen Doxyfile

clean:
	rm -rf $(BUILD_DIR) $(DEBUG_DIR) $(DOC_DIR)


build_dir:
	@mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/$(EXECUTABLE): $(BUILD_OBJECTS)
	$(CXX) $^ -o $@

$(BUILD_OBJECTS): $(BUILD_DIR)/%.o : $(SOURCE_DIR)/%$(SRC_EXT)
	@mkdir -p $(dir $@)
	$(CXX) -c $(WARN_FLAGS) $(OPTIMIZE_FLAGS) $< -o $@

debug_dir:
	@mkdir -p $(DEBUG_DIR)

$(DEBUG_DIR)/$(EXECUTABLE): $(DEBUG_OBJECTS)
	$(CXX) $^ -o $@

$(DEBUG_OBJECTS): $(DEBUG_DIR)/%.o : $(SOURCE_DIR)/%$(SRC_EXT)
	@mkdir -p $(dir $@)
	$(CXX) -c $(WARN_FLAGS) $(DEBUG_FLAGS) $< -o $@
