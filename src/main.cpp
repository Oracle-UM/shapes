#include "../include/Circle.hpp"
#include "../include/Rectangle.hpp"
#include "../include/Triangle.hpp"

#include <iostream>
#include <string>

using namespace std;

#define SEPARATOR ('\n' + string(30, '-') + '\n')

#ifdef __unix__
string _bold(string str) { return "\e[1m" + move(str) + "\e[0m"; }
#else
string _bold(string str) { return move(str); }
#endif

int main()
{
    try
    {
        Triangle my_triangle(6.666, 6.666, 6.666);
        cout << _bold("Printing a triangle\n");
        cout << my_triangle.show();
    }
    catch (const std::exception &e)
    {
        cerr << e.what() << '\n';
    }

    cout << SEPARATOR;

    try
    {
        Rectangle my_rectangle(3.2, 3.2);
        cout << _bold("Printing a rectangle\n");
        cout << my_rectangle.show();
    }
    catch (const std::exception &e)
    {
        cerr << e.what() << '\n';
    }

    cout << SEPARATOR;

    try
    {
        Circle my_circle(42.42);
        cout << _bold("Printing a circle\n");
        cout << my_circle.show();
    }
    catch (const std::exception &e)
    {
        cerr << e.what() << '\n';
    }
}
