#include "../include/Triangle.hpp"

#include <sstream>
#include <stdexcept>

#include <cmath>

using namespace std;

// Output triple
template <typename T1, typename T2, typename T3>
std::ostream &operator<<(std::ostream &out, const std::tuple<T1, T2, T3> &triple)
{
    return out
        << "("  << get<0>(triple)
        << ", " << get<1>(triple)
        << ", " << get<2>(triple)
        << ")";
}

Triangle::Triangle(double side_a, double side_b, double side_c)
{
    const auto checkNonPositiveSide = [](auto side,
                                         auto exception_message)
    {
        if (side <= 0)
        {
            throw invalid_argument(move(exception_message));
        }
    };

    const auto checkSideSumValid = [](auto first_side,
                                      auto second_side,
                                      auto third_side,
                                      auto exception_message)
    {
        if (first_side + second_side <= third_side)
        {
            throw logic_error(move(exception_message));
        }
    };

    checkNonPositiveSide(side_a, "Nonpositive triangle first side value");
    checkNonPositiveSide(side_b, "Nonpositive triangle second side value");
    checkNonPositiveSide(side_c, "Nonpositive triangle third side value");

    checkSideSumValid(
        side_a,
        side_b,
        side_c,
        "Sum of first and second side not greater than third side"
    );
    checkSideSumValid(
        side_a,
        side_c,
        side_b,
        "Sum of first and third side not greater than second side"
    );
    checkSideSumValid(
        side_b,
        side_c,
        side_a,
        "Sum of second and third side not greater than first side"
    );

    _side_a = side_a;
    _side_b = side_b;
    _side_c = side_c;

    __perimeter = -1;
    __area      = -1;

    __angle_ab = -1;
    __angle_ac = -1;
    __angle_bc = -1;

    __side_type  = static_cast<SideType>(-1);
    __angle_type = static_cast<AngleType>(-1);
}

double Triangle::perimeter() {
    if (-1 == __perimeter)
    {
        __perimeter = _side_a + _side_b + _side_c;
    }

    return __perimeter;
}

// Using Heron's Formula
double Triangle::area()
{
    if (-1 == __area)
    {
        const double semiperimeter = perimeter() / 2;
        __area = sqrt(
            semiperimeter *
            (semiperimeter - _side_a) *
            (semiperimeter - _side_b) *
            (semiperimeter - _side_c)
        );
    }

    return __area;
}

string Triangle::show()
{
    stringstream buffer_stream;

    buffer_stream
        << "Shape: "                << "triangle"      << '\n'
        << "Perimeter: "            << perimeter()     << '\n'
        << "Area: "                 << area()          << '\n'
        << "Side classification: "  << showSideType()  << '\n'
        << "Angle classification: " << showAngleType() << '\n'
        << "Sides: "                << sides()         << '\n'
        << "Angles: "               << angles()        << '\n';

    return buffer_stream.str();
}

tuple<double, double, double> Triangle::sides() const
{
    return { _side_a, _side_b, _side_c };
}

tuple<double, double, double> Triangle::angles()
{
    if (-1 == __angle_ab)
    {
        // Using the Law of Cosines
        const auto calcAngle = [](auto opposite_side,
                                  auto adj_side1,
                                  auto adj_side2)
        {
            return acos(
                (adj_side1     * adj_side1 +
                 adj_side2     * adj_side2 -
                 opposite_side * opposite_side
                ) / (2 * adj_side1 * adj_side2)
            ) * 180 / M_PI;
        };

        __angle_ab = calcAngle(_side_c, _side_a, _side_b);
        __angle_ac = calcAngle(_side_b, _side_a, _side_c);
        __angle_bc = 180 - __angle_ab - __angle_ac;
    }

    return { __angle_ab, __angle_ac, __angle_bc };
}

Triangle::SideType Triangle::sideType()
{
    if (-1 == static_cast<int>(__side_type))
    {
        __side_type =
            _side_a == _side_b ||
            _side_a == _side_c
                ? _side_a == _side_b &&
                  _side_a == _side_c
                      ? SideType::Equilateral
                      : SideType::Isosceles
                : SideType::Scalene;
    }

    return __side_type;
}

Triangle::AngleType Triangle::angleType()
{
    if (-1 == static_cast<int>(__angle_type))
    {
        if (SideType::Equilateral == __side_type)
        {
            __angle_type = AngleType::Acute;
        }

        else
        {
            angles();  // Making sure angles are calculated & set

            __angle_type =
                __angle_ab > 90 ||
                __angle_ac > 90 ||
                __angle_bc > 90
                    ? AngleType::Obtuse
                    : __angle_ab == 90 ||
                      __angle_ac == 90 ||
                      __angle_bc == 90
                          ? AngleType::Right
                          : AngleType::Acute;
        }
    }

    return __angle_type;
}

string Triangle::showSideType()
{
    switch (sideType())
    {
    case SideType::Scalene:
        return "scalene";
    case SideType::Isosceles:
        return "isosceles";
    case SideType::Equilateral:
        return "equilateral";
    default:
        throw invalid_argument("Invalid triangle side type");
    }
}

string Triangle::showAngleType()
{
    switch (angleType())
    {
    case AngleType::Acute:
        return "acute";
    case AngleType::Right:
        return "right";
    case AngleType::Obtuse:
        return "obtuse";
    default:
        throw invalid_argument("Invalid triangle angle type");
    }
}
