#include "../include/Circle.hpp"

#include <sstream>
#include <stdexcept>

#include <cmath>

using namespace std;

Circle::Circle(double radius)
{
    if (radius <= 0)
    {
        throw invalid_argument("Nonpositive radius value");
    }

    _radius = radius;

    __perimeter = -1;
    __area      = -1;
}

double Circle::perimeter()
{
    if (-1 == __perimeter)
    {
        __perimeter = 2 * M_PI * _radius;
    }

    return __perimeter;
}

double Circle::area()
{
    if (-1 == __area)
    {
        __area = M_PI * _radius * _radius;
    }

    return __area;
}

string Circle::show()
{
    stringstream buffer_stream;

    buffer_stream
        << "Shape: "     << "circle"    << '\n'
        << "Perimeter: " << perimeter() << '\n'
        << "Area: "      << area()      << '\n'
        << "Radius: "    << _radius     << '\n';

    return buffer_stream.str();
}
