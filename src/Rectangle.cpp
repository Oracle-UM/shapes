#include "../include/Rectangle.hpp"

#include <sstream>
#include <stdexcept>

using namespace std;

Rectangle::Rectangle(double width, double height)
{
    const auto checkNonPositiveSide = [](auto side, auto message)
    {
        if (side <= 0)
        {
            throw invalid_argument(move(message));
        }
    };

    checkNonPositiveSide(width,  "Nonpositive rectangle width value");
    checkNonPositiveSide(height, "Nonpositive rectangle height value");

    _width  = width;
    _height = height;

    __perimeter = -1;
    __area      = -1;

    __is_square = -1;
}

double Rectangle::perimeter()
{
    if (-1 == __perimeter)
    {
        __perimeter = 2 * (_width + _height);
    }

    return __perimeter;
}

double Rectangle::area()
{
    if (-1 == __area)
    {
        __area = _width * _height;
    }

    return __area;
}

string Rectangle::show()
{
    stringstream buffer_stream;

    buffer_stream
        << "Shape: "     << "rectangle" << (isSquare() ? " (square)\n" : "\n")
        << "Perimeter: " << perimeter() << '\n'
        << "Area: "      << area()      << '\n'
        << "Width: "     << _width      << '\n'
        << "Height: "    << _height     << '\n';

    return buffer_stream.str();
}

bool Rectangle::isSquare()
{
    if (-1 == __is_square)
    {
        __is_square = _width == _height;
    }

    return __is_square;
}
